# stx-mirror-rs

This program downloads files from the StarlngX mirror. It saves the files in the current directory with the same folder structure than the server.

## Building

Just clone the code and run `cargo build --release`

## Running

Run with `cargo run` or call the binary directly.

## Using docker

There is an image `erichcm/stx-mirror-rs:0.1` and it can be run in this way:

```
docker run -ti --user=$(id -u):$(id -g) -v $(pwd):/work erichcm/stx-mirror-rs
```

This will store the files in the current directory.

Alternatively, you can specify a URL to download the files in case you want an specific mirror.

```
docker run -ti --user=$(id -u):$(id -g) -v $(pwd):/work erichcm/stx-mirror-rs --url http://mirror.starlingx.cengn.ca/mirror/starlingx/r2018.10/centos/2018.10.0/inputs/
```

## Issues

The container cannot be stopped by ctrl+c. It needs an explicit `docker kill <container-name>` command.
