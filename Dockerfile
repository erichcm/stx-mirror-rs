FROM scratch

COPY target/x86_64-unknown-linux-musl/release/stx-mirror-rs /stx-mirror-rs

WORKDIR /work

ENTRYPOINT ["/stx-mirror-rs"]
