extern crate reqwest;
extern crate select;
extern crate structopt;
extern crate threadpool;

use select::document::Document;
use select::predicate::Name;
use threadpool::ThreadPool;
use structopt::StructOpt;

use std::path::PathBuf;
use std::io::copy;
use std::fs::{create_dir_all, File};
use std::sync::mpsc::channel;
use std::env;
use std::io;

#[derive(Debug)]
enum MirrorError {
    FetchError(reqwest::Error),
    FileError(io::Error),
}

impl From<io::Error> for MirrorError {
    fn from(error: io::Error) -> Self {
        MirrorError::FileError(error)
    }
}


#[derive(StructOpt, Debug)]
#[structopt(name = "stx-mirror-rs",
            about = "A tool to download a pre populated StarlingX mirror.")]
struct Opt {
    // URL to download
    #[structopt(short = "u", long = "url")]
    url: Option<String>,
}

#[derive(Debug, Clone)]
struct Item {
    // The name of the file
    name: String,
    // The url to download the file
    url: String,
    // The path to save the file
    path: PathBuf,
}

impl Item {
    pub fn new(name: String, url: String, path: PathBuf) -> Item {
        Item {
            name,
            url,
            path,
        }
    }
}

#[derive(Debug, Clone)]
struct Downloader {
    client: reqwest::Client,
}

impl Downloader {
    pub fn new() -> Downloader {
        let proxy_env = env::var("http_proxy").
        or_else(|_| env::var("HTTP_PROXY")).ok();

        if let Some(proxy) = proxy_env {
            let p = reqwest::Proxy::http(&proxy).unwrap();
            Downloader {
                client: reqwest::Client::builder()
                .proxy(p)
                .build()
                .unwrap(),
            }
        } else {
            Downloader {
                client: reqwest::Client::new(),
            }
        }
    }

    fn get(&self, url: &str) -> Result<reqwest::Response, MirrorError> {
        match self.client.get(url).send() {
            Ok(v) => Ok(v),
            Err(e) => Err(MirrorError::FetchError(e)),
        }
    }
}

// Extracts the save path from the url.
fn get_path_from_url(url: &str) -> Option<String> {
    match url.rfind("inputs/") {
        Some(p) => {
            let mut path = url[p + 7..].to_string();
            path = path.replace("SRPMS", "Source");
            path = path.replace("RPMS", "Binary");
            Some(path)
        },
        None => None,
    }
}


// Checks if string is folder
fn is_folder(url: &str) -> bool {
    url.ends_with("/") && url != "../"
}

// Recursively gets all the files and inserts data in a vector
fn get_links(url: &str, mut items: &mut Vec<Item>) {
    let client = Downloader::new();
    let res = client.get(url).unwrap().text().unwrap();
    let doc = Document::from(res.as_str());
    // Iterate over all links in web pages
    for link in doc.find(Name("a")) {
        if let Some(l) = link.attr("href") {
            if is_folder(l) {
                get_links(&format!("{}{}", url, l), &mut items);
            } else if l != "../" {
                // if path cannot be get, then use current directory
                let path = get_path_from_url(url)
                    .unwrap_or("./".to_string());
                let item = Item::new(l.to_string(),
                                     format!("{}{}", url, l),
                                     PathBuf::from(path));
                items.push(item);
            }
        }
    }
}


fn download_file(item: &Item) -> Result<(), MirrorError>{
    let fname = item.path.join(&item.name);
    if fname.exists() {
        println!("Skipping {}", item.name);
        return Ok(());
    }
    println!("Downloading {}", item.name);
    let client = Downloader::new();
    let mut res = client.get(&item.url)?;

    if ! item.path.exists() {
        create_dir_all(&item.path)?;
    }
    let mut dest = File::create(fname)?;
    copy(&mut res, &mut dest)?;

    Ok(())
}

fn main() {
    let opt = Opt::from_args();
    let mut baseurl: String;

    if let Some(url) = opt.url {
        baseurl = url;
    } else {
        baseurl = "http://mirror.starlingx.cengn.ca/mirror/\
                   starlingx/master/centos/latest_build/inputs/"
            .to_string();
    }

    let mut items: Vec<Item> = vec![];
    let pool = ThreadPool::new(4);
    let (tx, rx) = channel();
    println!("Will download from: {}", baseurl);
    println!("Gathering information from server...");
    get_links(&baseurl, &mut items);
    println!();
    for i in &items {
        let i = i.clone();
        let tx = tx.clone();
        // Launching workers
        pool.execute(move || {
            tx.send(download_file(&i))
                .expect("Could not sync");
        });
    }

    // Just wait for workers to finish.
    drop(tx);
    for r in rx.iter() {
        if let Err(e) = r {
            println!("Error downloading {:?}", e);
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn extract_path() {
        let url = "/centos/latest_build/inputs/downloads/";
        let path = url.rfind("inputs/").unwrap();
        assert_eq!(&url[path + 7..], "downloads/");
    }

    #[test]
    fn get_filename_from_url() {
        let url = "https://some/url/to/file.rpm";
        let filename = url.rfind("/").unwrap();
        assert_eq!(&url[filename + 1..], "file.rpm");
    }
}
